# Max's OmniFocus Workflow

This repo contains information about how Max runs his OmniFocus workflow and mutliple taskpaper templates to use in automated scripts.

## New Milestone

1. At the start of a new milestone, create a new folder under `Work -> Milestones` with the name of the milestone (e.g. `13.2`)
2. Make sure it contains a `General Links` project with links to the milestone issue and any milestone-wide issues.
3. Make sure it contains a `Misc Tasks` project which contains any smaller tasks and can act as an inbox for any mid-milestone additions.

### Example
```taskpaper
- General Links @parallel(true) @autodone(false) @flagged
	- 13.2 Board - https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17121 @parallel(true) @autodone(false)
	- Next Up - Refinement time @parallel(true) @autodone(false)
- Misc Tasks @parallel(true) @autodone(false) @due(2020-07-13 15:00)
	- Some random task @parallel(true) @autodone(false)
```
![](img/general-links.png)

## Milestone Deliverables

Each milestone usually contains a set of larger deliverables or development bugs. For each of these, usually at the beginning of a milestone, create a new project.

Follow the general principle:

* Issue maps 1:1 with project.
* MR maps 1:1 with task.

Most deliverables fall in to one of these two categories:

### Issue with single MR

* The project name is the name of the Issue.
* The project is a sequential project.
* The tasks are the reviews that are required by other engineers.

```taskpaper
- Refactor author_name from AuditEvent Details @parallel(false) @autodone(false) @tags(Work : Feature)
	- Initial Review @parallel(true) @autodone(false)
	- Database Review @parallel(true) @autodone(false)
	- Maintainer Review @parallel(true) @autodone(false)
```
![](img/single-mr.png)

### Issue with multiple MRs

Sometimes an Issue can become multiple MRs, in this case:

* The project name is (again) the name of the Issue.
* The project is a sequential project.
* Each parent task is an MR.
* Each parent task is sequential.
* Each subtask of that MR is the name of a review required.

```taskpaper
- Compliance Frameworks to API @parallel(true) @autodone(false) @tags(Work : Feature)
	- Add compliance framework project labels to Project API - https://gitlab.com/gitlab-org/gitlab/-/issues/215157 @parallel(true) @autodone(false)
	- GraphQL MR @parallel(false) @autodone(false) @context(Work : Merge Request) @tags(Work : Merge Request)
		- Initial Review @parallel(true) @autodone(false)
		- Maintainer Review @parallel(true) @autodone(false)
	- REST @parallel(false) @autodone(false) @context(Work : Merge Request) @tags(Work : Merge Request)
		- Initial Review @parallel(true) @autodone(false)
		- Maintainer Review @parallel(true) @autodone(false)
```

![](img/multiple-mrs.png)